# abas-dashboard-state
The abas-dashboad-state supplies dashboards with environment, url parameters and other meta information. abas-dashboard-state can be extended by writing information modules. All modules are js modules and can be found in the modules folder. The state should after a succsefull user authentication and before loading the dashboard. State information should be present when the aba-dashboard is loaded. There are two ways to retrieve state information.
The state is automatically published via message bus but is also availabe as promised result.


# Loading the state
## Using the web component
```html
<abas-dashboard-state></abas-dashboard-state>
```
## Main usage of the API
Main use case is to wait for initial state data to be set up. The state data is published via the message bus as a side effect of the setupState call. Following components and features can easily retrieve the data via the abas-dashboard-state-meta component or by message bus subscription.
```javascript
// authentication done
let state = new AbasDashboardState({
    configuration: config,
    urlSearchString: document.location.search });
await state.setupState()
// load application here...
```
## Using the Promise API 
#### Function call
Mainly for testing and maybe later development. State data is the promised result of the setupState call.
```javascript
// authentication done
let state = new AbasDashboardState({
    configuration: config,
    urlSearchString: document.location.search})
    .setupState()
    .then( (stateInformation) => {
        // load application here...
    })
```
#### Example stateInformation
```json
[
    {
        "module": "UserInfo",
        "data": [
            {
                "client": {
                    "id": "erp",
                    "description": "The description of erp",
                    "href": "/mw/r/erp",
                    "environment": {
                        "erpClientName": "docker-erp",
                        "erpVersion": "abas Version 2018r4n10",
                        "erpVersionNumber": "2018r4n10",
                        "erpTimeZone": "Etc/UTC",
                        "erpDateTime": "2019-05-02T13:33:38Z",
                        "erpOperatingLanguages": [
                            {
                                "code": "de",
                                "erpCode": "de",
                                "erpCodeShort": "D",
                                "languageTag": "de"
                            },
                            {
                                "code": "de_CH",
                                "erpCode": "de",
                                "erpCodeShort": "D",
                                "languageTag": "de"
                            },
                            {
                                "code": "en_US",
                                "erpCode": "en_US",
                                "erpCodeShort": "A",
                                "languageTag": "en-US"
                            },
                            {
                                "code": "de_AT",
                                "erpCode": "de",
                                "erpCodeShort": "D",
                                "languageTag": "de"
                            },
                            {
                                "code": "de_DE",
                                "erpCode": "de",
                                "erpCodeShort": "D",
                                "languageTag": "de"
                            }
                        ],
                        "erpFeatures": [
                            {
                                "id": "BLOCKING_ACTIONS",
                                "supported": false
                            },
                            {
                                "id": "BLOCKING_DIALOGS",
                                "supported": false
                            }
                        ]
                    },
                    "default": false
                },
                "data": {
                    "_type": "ErpUserInfo",
                    "code": {
                        "links": null,
                        "value": "eMail@address.something"
                    },
                    "email": {
                        "links": null,
                        "value": "eMail@address.something"
                    },
                    "name": {
                        "links": null,
                        "value": "eMail@address.something"
                    },
                    "loginName": {
                        "links": null,
                        "value": ""
                    },
                    "account": {
                        "text": "(1,2,3)",
                        "value": "erp"
                    },
                    "employee": {
                        "text": "1234567",
                        "value": "(5,6,7)"
                    }
                }
            },
            {
                "client": {
                    "id": "hurleburlebutz",
                    "description": "Another client",
                    "href": "/mw/r/hurleburlebutz",
                    "environment": {
                        "erpClientName": "docker-erp",
                        "erpVersion": "abas Version 2018r4n10",
                        "erpVersionNumber": "2018r4n10",
                        "erpTimeZone": "Etc/UTC",
                        "erpDateTime": "2019-05-02T13:33:38Z",
                        "erpOperatingLanguages": [
                            {
                                "code": "de",
                                "erpCode": "de",
                                "erpCodeShort": "D",
                                "languageTag": "de"
                            },
                            {
                                "code": "de_CH",
                                "erpCode": "de",
                                "erpCodeShort": "D",
                                "languageTag": "de"
                            },
                            {
                                "code": "en_US",
                                "erpCode": "en_US",
                                "erpCodeShort": "A",
                                "languageTag": "en-US"
                            },
                            {
                                "code": "de_AT",
                                "erpCode": "de",
                                "erpCodeShort": "D",
                                "languageTag": "de"
                            },
                            {
                                "code": "de_DE",
                                "erpCode": "de",
                                "erpCodeShort": "D",
                                "languageTag": "de"
                            }
                        ],
                        "erpFeatures": [
                            {
                                "id": "BLOCKING_ACTIONS",
                                "supported": false
                            },
                            {
                                "id": "BLOCKING_DIALOGS",
                                "supported": false
                            }
                        ]
                    },
                    "default": false
                },
                "data": {
                    "code": {
                        "links": null,
                        "value": "eMail@address.something"
                    },
                    "email": {
                        "links": null,
                        "value": "eMail@address.something"
                    },
                    "name": {
                        "links": null,
                        "value": "eMail@address.something"
                    },
                    "loginName": {
                        "links": null,
                        "value": ""
                    },
                    "account": {
                        "text": "(1,2,3)",
                        "value": "hurleburlebutz"
                    },
                    "employee": {
                        "text": "1234567",
                        "value": "(5,6,7)"
                    }
                }
            }
        ]
    },
    {
        "module": "URLContextPump",
        "data": [
            {
                "topic": "3:2",
                "data": {
                    "id": "(336,3,0)"
                }
            },
            {
                "topic": "2:1",
                "data": {
                    "id": "(200,2,0)"
                }
            }
        ]
    }
]
```

# Retrieving state data
## Using the web component
Most common use with polymer 2. Simply add an observer on the _userInfo property in this example.  
```html
<abas-dashboard-state-meta user-info={{_userInfo}}></abas-dashboard-state-meta>
```
## Accessing the dashboard state
```javascript
let companyUserList = await AbasDashboardState.userInfo
let defaultCompany =  await AbasDashboardState.defaultCompany
```

## Getting the state via message bus
```javascript
postal.subscribe({
    channel: AbasDashboardState.channel,
    topic: AbasDashboardState.topic.userInfo,
    callback: (userInfoList) => {
        // do somthing with user/company info hre
    }
})
```
## Example of userInfo list via bus
```json
[
    {
        "client": {
            "id": "erp",
            "description": "The description of erp",
            "href": "/mw/r/erp",
            "environment": {
                "erpClientName": "docker-erp",
                "erpVersion": "abas Version 2018r4n10",
                "erpVersionNumber": "2018r4n10",
                "erpTimeZone": "Etc/UTC",
                "erpDateTime": "2019-05-02T13:33:38Z",
                "erpOperatingLanguages": [ ... ],
                "erpFeatures": [ ... ]
            },
            "default": false
        },
        "data": {
            "_type": "ErpUserInfo",
            "code": {
                "links": null,
                "value": "eMail@address.something"
            },
            "email": {
                "links": null,
                "value": "eMail@address.something"
            },
            "name": {
                "links": null,
                "value": "eMail@address.something"
            },
            "loginName": {
                "links": null,
                "value": ""
            },
            "account": {
                "text": "(1,2,3)",
                "value": "erp"
            },
            "employee": {
                "text": "1234567",
                "value": "(5,6,7)"
            }
        }
    }
]
```

## Extending state
Extending abas-dashboard-state with new environment information is done by writing js module. The folder for modules is named module.
### Modules must met following convetions
* a module should be shaped as javascript class. Since we must work around es6 imports place your class in window.abas.dashboardModule.YourClass
* all work is done within a method named 'work' which returns a collection of Promises which resolve with a the state information
* State information is published via the message bus. Channel is AbasDashboardState.channel. The topic is freely choosable but registred in AbasDashboad.topic
* While the state information published via the bus system to a specific topic the Promise result shape is like:
```javascript
 {module: this.constructor.name, data: stateInformationData}
```